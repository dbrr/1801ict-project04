
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

/** This class is the main page for the user interface.
 *
 * @author Jordan Day
 */
public class MainGUI extends JFrame {
	/** This holds all of the info for a student profile */
	StudentProfile[] profileList = new StudentProfile[4];

	private static JPanel titleBox = new JPanel(),
						  box1 = new JPanel(),
						  box2 = new JPanel(),
					  profile0 = new JPanel(),
					  profile1 = new JPanel(),
					  profile2 = new JPanel(),
					  profile3 = new JPanel();

	private static JButton
		newProfileBtn0 = new JButton("New Profile"),
		newProfileBtn1 = new JButton("New Profile"),
		newProfileBtn2 = new JButton("New Profile"),
		newProfileBtn3 = new JButton("New Profile");

	// Constructor
	/** Class constructor */
	MainGUI() { 
		// LOAD DATA FROM FILE INTO HERE:
		
		// GUI:
		super("Student Grade Calculator"); 
		setBounds(100, 100, 500, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container con = this.getContentPane();
		con.add(titleBox);
		titleBox.add(box1);
		titleBox.add(box2);
		// customise panel here:

		// set layout
		titleBox.setLayout(new BoxLayout(
					titleBox, BoxLayout.Y_AXIS));
		titleBox.setAlignmentX(
				Component.CENTER_ALIGNMENT);
		box1.setLayout(new BoxLayout(
					box1, BoxLayout.Y_AXIS));
		box2.setLayout(new BoxLayout(
					box2, BoxLayout.Y_AXIS));
		profile0.setLayout(new BoxLayout(
					profile0, BoxLayout.Y_AXIS));
		profile1.setLayout(new BoxLayout(
					profile1, BoxLayout.Y_AXIS));
		profile2.setLayout(new BoxLayout(
					profile2, BoxLayout.Y_AXIS));
		profile3.setLayout(new BoxLayout(
					profile3, BoxLayout.Y_AXIS));

		box1.setBorder(new EmptyBorder(5, 5, 5, 5));
		box1.add(new JLabel("Student Grade Calculator"));

		// Box 2 will be used to create new profiles
		//	Maybe can use drop down to delete
		//		eg. shows name on drop down then a delete button
		box2.setBorder(new EmptyBorder(5, 5, 15, 15));
		box2.add(new JLabel("Another label"));
		setVisible(true);

		// profile0
		box2.add(profile0);
		if (profileList[0] == null) {
			// show an <empty> box
			profile0.setBorder(BorderFactory.createLineBorder(
					Color.black));
			profile0.add(new JLabel("profile0 empty"));
			profile0.add(newProfileBtn0);
		}
		else {
			// show box with <profile0> name
			profile0.setBorder(BorderFactory.createLineBorder(
					Color.black));
			profile0.add(new JLabel("get profile0 name"));
		}

		// profile1
		box2.add(profile1);
		if (profileList[1] == null) {
			// show an <empty> box
			profile1.setBorder(BorderFactory.createLineBorder(
					Color.black));
			profile1.add(new JLabel("profile1 empty"));
			profile1.add(newProfileBtn1);
		}
		else {
			// show box with <profile1> name
			profile1.setBorder(BorderFactory.createLineBorder(
					Color.black));
			profile1.add(new JLabel("get profile1 name"));
		}

		// profile2
		box2.add(profile2);
		if (profileList[2] == null) {
			// show an <empty> box
			profile2.setBorder(BorderFactory.createLineBorder(
					Color.black));
			profile2.add(new JLabel("profile2 empty"));
			profile2.add(newProfileBtn2);
		}
		else {
			// show box with <profile2> name
			profile2.setBorder(BorderFactory.createLineBorder(
					Color.black));
			profile2.add(new JLabel("get profile2 name"));
		}

		// profile3
		box2.add(profile3);
		if (profileList[3] == null) {
			// show an <empty> box
			profile3.setBorder(BorderFactory.createLineBorder(
					Color.black));
			profile3.add(new JLabel("profile3 empty"));
			profile3.add(newProfileBtn3);
		}
		else {
			// show box with <profile3> name
			profile3.setBorder(BorderFactory.createLineBorder(
					Color.black));
			profile3.add(new JLabel("get profile3 name"));
		}
	}

//	private static void addListeners() {
//		// TODO:
//		// newProfileBtn0
//		// newProfileBtn1
//		// newProfileBtn2
//		// newProfileBtn3
//	}
}
