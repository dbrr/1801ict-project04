
/**
 * This class utilizes the Student Profile program logic.
 *
 * @author Jordan Day
 */
public class StudentProfile {
	// the id should be same as where it's located in the array
	private int id; // to make every profile unique
	private String firstName;
	private String lastName;
	//private int gpa;
	//private int[] results;

	private int courseCount;
	
	// DEBUGGING:
	// change to private and add functions for access later
	/** The user is able to add and delete courses. */
	public StudentCourse[] courseList = new StudentCourse[50];
	// TODO: Getters and setters for accessing classes from here
	
	// public variables

	// constructor
	/** Constructor 
	 * @param id 	This should be the index in the list of which it is found in it's parent's array.
	 * @param firstName		The first name of the student.
	 * @param lastName		The last name of the student.
	 */
	StudentProfile(String first, String second, int n) {
		id = n;
		firstName = first;
		lastName = second;
	}

	/** Allows to change the id value
	 * @param idSetter 	called in the constructor of this function to edit the id value.
	 * @return void
	 */
	public void setID(int idSetter) {
		id = idSetter;
	}	
	/** Returns the id variable of the class
	 * @return id
	 */
	public int getID() {
	// returns the id of the class
		return(id);
	}

	/** This method will return a string of the first name
	 * + last name according to thier profile names.
	 * @param args unused.
	 * @return String
	 */
	public String getNames() {
	// returns a string of the first and last name
		return (firstName.concat(" ").concat(lastName));
	}

	/* these might not be needed due to above function
	getNameFirst() {
	}

	getNameLast() {
	}
	*/


	/*

	public getResults() {
	}
	private calculateResults() {
	}
	getCourses() {
	// this will print the names of the courses the student 
	// is enrolled in.
	}
	*/

	/**
	 * This method is designed to create a new course add append it to the courseList array.
	 * @param name defines the name of the course.
	 * @return boolean will return true if sucessful, false otherwise.
	 */
	public boolean addCourse(String name) {
	// returns true if successful
		courseCount++;
		courseList[courseCount] = new StudentCourse(name, id);
		return true;
	}

	/**
	 * This method is designed to delete a new course add from the courseList array and sort the list if required.
	 * @param name defines the name of the course.
	 * @return boolean will return true if sucessful, false otherwise.
	 */
	public boolean removeCourse(String name) {
	// returns 0 if successful
		// TODO: maybe a function to search for courseName, return index
		// TODO: after may require adjusting array
		// 		Maybe look into something like a c++ vector
		return false;
	}
}
