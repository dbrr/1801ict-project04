/*
 * file: 	projectGUI.java
 * author:	Jordan Day
*/

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;


public class ProjectGUI {

	// the app name
	private static final String APP_NAME = "ProjectGUI";

	// The frame
	private static JFrame frame = new JFrame(APP_NAME);

	private static final String FILE_NAME_PROFILE =		APP_NAME + "profile.txt";
	private static final String FILE_NAME_COURSES =		APP_NAME + "courses.txt";
	private static final String FILE_NAME_ASSESSMENT =	APP_NAME + "assessment.txt";
	
	// Student profiles
	private StudentProfile[] profileList = new StudentProfile[4];  

	public static void main(String[] args) {
		// Load from storage here

		layoutComponents();
		addListeners();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
	}

	// Buttons:
	private static JButton
		newProfile		= new JButton("new profile"),
		delProfile		= new JButton("Delete"),
		editProfile		= new JButton("Edit"),
		newCourse		= new JButton("New Course"),
		delCourse		= new JButton("Delete"),
		editCourse		= new JButton("Edit Name"),
		newAssessment	= new JButton("New assessment item"),
		delAssessment	= new JButton("Delete"),
		editAssessment	= new JButton("Edit");

	private static void layoutComponents() {
		JPanel box0 = new JPanel(),
		       box1 = new JPanel(),
		       box2 = new JPanel(),
		       box3 = new JPanel();
		box0.setLayout(new BoxLayout(box0, BoxLayout.Y_AXIS));
		box1.setLayout(new BoxLayout(box1, BoxLayout.X_AXIS));
		box2.setLayout(new BoxLayout(box2, BoxLayout.Y_AXIS));
		box3.setLayout(new BoxLayout(box3, BoxLayout.X_AXIS));
		super("Student Results Calculator");
		frame.add(box0);
		box0.add(box1);
		box0.add(box2);
		box0.add(box3);
		box1.setAlignmentX(
				Component.CENTER_ALIGNMENT);
		box2.setAlignmentX(
				Component.CENTER_ALIGNMENT);
		box3.setAlignmentX(
				Component.CENTER_ALIGNMENT);
//		box1.setBorder(new EmptyBorder(5, 5, 5, 5));
//		box2.setBorder(new EmptyBorder(5, 5, 5, 5));
//		box3.setBorder(new EmptyBorder(5, 5, 5, 5));
//		box1.add(new JLabel("Name");
//		box1.add(
	}

	private static void addListeners() {
		
	}
}
