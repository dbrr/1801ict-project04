
import java.util.*;

/**
 * This program is designed to create a maximum of 4 
 * profiles of students the profiles are designed to
 * calculate the total grade that a student will obtain.
 * 
 * @author Jordan Day
 */
public class Main {
	public static void main(String[] args) {
		// Main
		new MainGUI();

// ---------------------------------------------------------
// HARD CODING THE CLASSES
// 		anything onwards is useless:
// 		staying here for refrence for now
// ---------------------------------------------------------
//		int index = 0;
		
//		profileList[index] = new StudentProfile("John", "Doe", index);
//		profileList[index+1] = new StudentProfile("Jane", "sMith", index+1);

		// adding a new course
//		profileList[index].addCourse("maths");
//		profileList[index].addCourse("english");
//
//		profileList[index+1].addCourse("chem");
//		profileList[index+1].addCourse("physics");

		// print name and id
//		System.out.println("name: " + profileList[index].getNames() + ", id: " + profileList[index].getID());
		// print course names
//		System.out.println(profileList[index].courseList[0].getCourseName() + " " + profileList[index].courseList[1].getCourseName());

		// print name and id
//		System.out.println("name: " + profileList[index+1].getNames() + ", id: " + profileList[index+1].getID());
		// print course names
//		System.out.println(profileList[index+1].courseList[0].getCourseName() + " " + profileList[index+1].courseList[1].getCourseName());
		

	}
}
