# Conceptual Questions

1. 3 types of errors:
	1. **Syntax Errors:**
		Any problems which are detected at compile time
		_Example:_
			* Calling a method that doesn't exist.
			* Type errors.
	2. **Logical Errors:**
		Errors made by the programmer that cause the program to not perform as required.
		These errors can be avoided with careful design or with sufficient testing.
		_Example:_ 
			* Never ending while loop due to no termination.
	3. **Run-time errors:**
		Problems that are detected during run time.
		_Example:_ 
			* Out of disk space
			* Can't read a file due to insufficient permissions.
			* Network Connection loss.
2. Error Handling Strategies
	1. **Defensive programming:**
		Creating validation checks during runtime.
		_Example:_ A String in a program cannot be more than 10 characters due to some kind of restraint in the program.
	2. **Programming by Contract:**
		Using precondition and postconditions to create a 'contract'.
		If the precondition don't work, its not the other person's fault that it doesn't work.
		_Example:_ 
			precondition: You give me at least 2 numbers.
			postcondition: I will add them together.
	3. **Unavoidable, unexpected errors:**
		Runtime errors caused by unexpected events where a program could recover.
		_Example:_ A program is missing a file. Defensive programming can solve this.

3. Throw-try-catch
_Example:_
```c++
try {
	// Code that could throw an exception
	throw someOtherException();
}
catch (const someException& e) {
	// Code that executes when exception of type
	// someException is thrown.
}
catch (...) {
	// Catch othe exceptions
}
```

4. Discuss characteristics of:
	* Sets
		* An abstract data type.
		* Store a collection of things
		* No order
		* No duplicates
	* Bags
		* Like sets but allow duplicates
	* Lists
		* Are a collection of elements of the same type
		* Can grow or shrink as elements are inserted/removed
		* Ordering not required
	* Stacks
		* Sequences intended to use only in a certain way
			* pushing
			* popping
			* peeking
			* finding out the how many things are in the stack
	* Queues
		* Are sequences that we intend to use only:
			* finding out how much stuff is inside
			* add something to the end
			* peeking at the thing at the front
			* dequeue - remove an item from the front
	* Deques
		* No defined front or back
	* Priority Queues
		* Like queues with extra feature where ever element has a priority that gets them to the front faster than an element with lesser priority
	* Maps
		* Abstract data type
		* Store and retrieve values by their unique key

5. Compare and constrast between arrays and linked lists, also singly linked lists and doubly linked lists
	* Singly linked list is based on objects where each object in the list contains a refrence to the next object
	* Doubly linked list is based on objects where each object in the list contains a refrence to both the next object and the one before.

6. Discuss concept of hash tables, compare and contrast with arrays and linked lists
	* Very fast way to store lots of values
	* Is a combination of an array and a linked list

7. What is hash collision? How could this be resolved?
	* Hash tables evenly distribute values to value having multiple values in the list
	* Could be resolved by taking note that there is more than 1 of that value in the table.

8. What is a generic class? How is this implemented in Java and C++
	* It is always declaring that it's element type will always be the same.
	* C++ this is done by the compiler creating new course files with the actural types substituted.

9. Compare the two ways of implementing generic classes in Java: using objects and using parameterised classes.
	* Java parameterised class has a type parameter that is substituted for an actual type at compile time
	* Type parameters are written between angle brackets < > after the class name.

10. What are the concepts of iterators? How are the iterators implemented in Java and in C++
	* iterators are used for common tasks of looping through a sequence, higher level languages use these to simplify this task.
```c++
vector<Blob> bs = ...
for (Blob b : bs){
	//do something
}
```

11. What are static member class, a dynamic member class, a local class, and a anonymous local class?
	* **Static Member classes:**
		* A class defined as a member (inside) of another class with the modifier _static_. Does not have access to dynamic members.
	* **Dynamic member classes:**
		* A class defined as a member (inside) of another class, without the static modifier. It has access to the static and dynamic members of its parent class, even the private ones
	* **Local Classes:**
		* Local classes are local i nthe same sense local variables are local, declared inside a method. Syntax is same but without any modifiers.
	* **Anonymous local classes:**
		* Its single instance instantiated, all in one expression. this expression can be used anywhere an expression of that type can be used.



# Project Description

Project 4 will be an implementation of a student results management system.

The program will create a profile for a student. The student will then be able to add classes that the student is assigned to. In addition to this, the student will be able to add assessment items and the weighting of the item towards the current mark, the program will be able to calculate how much percent is needed to get a certain grade.

## Classes
drafting  
UPLOAD UML LATER

The class that holds all the stundents info  
```
StudentProfile
	- id: int
	- firstName: string
	- lastName:	string
	- results[]: int
	- gpa: int
	- courseList[]: list of StudentCourse
	
	+ setId(number: int) 
		this set the id of student profile to the index that 
		it can be found, this will assist with creating and deleting.
		Additionally will require child classes to take note of this
		value too.
	+ setName(firstName: String, lastName: String)
		This will allow changing of the the users first and last
		name.
	+ addToResults()
		This will access the results array and append result values.
	+ deleteResults(index: int)
		This will access the results array and delete values.
	+ addCourse(courseName: String, parentID: int): boolean
		This will add a course to the courseList
		sets the name of the course and sends the indexID of
		this StudentProfile. The indexID will be used when reading
		from storage of a JSON file.
		Returns 1 if successful, 0 if error
	+ removeCourse(index: int): boolean
		confirm if user wants to do this
		confirm if name exists
		Will remove a course at index specified.
		Returns 1 if successful, 0 if not

	+ getID(): int
		returns the ID of current class
	+ getNames(): string
		returns a string of the first and last name
	+ getResults(): [] String
	- calculateResults(): void
	+ getCourses(): [] String
		Returns the names off all the courses the student is
		enrolled in.
```

This class will hold all course info for 1 course extends from StudentProfile.
```
StudentCourse
	- courseName: string
	- totalResult: string
	- assessmentItems[]: list of AssessmentItems
	- maxWeight: int
		This variable will be used in AssessmentItem class
		for creating an assessment item the total weight of the
		item cannot excceed this value.
	+ getCourseName()
	+ getTotalResult()
	+ getMaxWeight()
	+ setMaxWeight()
	+ setTotalResult()
	+ setCourseName()
	+ addAssessmentItem()
	+ delAssessmentItem()
```

This will hold the assessment infomation extends from StudentCourse
```
AssessmentItem
	- itemWeight
	- result
	- maxWeight
	+ getResult()
```
